package Adapter;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Object1 implements IObject {
    @Override
    public void doAction() {
        System.out.println("Some Actions of " + this.getClass().getSimpleName() + " Happens in here..");
    }
}
