package Adapter;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        IWorker worker = new Worker1();
        IWorker _worker = new Worker2();
        worker.work();
        _worker.work();
    }
}
