package Adapter;

/**
 * Created by Deniz on 12.04.2016.
 */
public interface IWorker {
    void work();
}
