package Adapter;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Worker2 implements IWorker {
    private IObject adaptee = new Object2();

    @Override
    public void work() {
        adaptee.doAction();
    }
}
