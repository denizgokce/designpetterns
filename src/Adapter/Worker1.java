package Adapter;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Worker1 implements IWorker {
    private IObject adaptee = new Object1();

    @Override
    public void work() {
        adaptee.doAction();
    }
}
