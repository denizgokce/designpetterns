package Decorater;

/**
 * Created by Deniz on 12.04.2016.
 */
public class AgeData extends SampleDataDecorater {

    public AgeData(SampleData sampleData) {
        super(sampleData);
    }

    @Override
    String GetData() {
        return sampleData.GetData() + " " + "23";
    }
}
