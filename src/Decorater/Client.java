package Decorater;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        SampleData currData = new FormattedSampleData();
        System.out.println(currData.GetData());
        currData = new AgeData(currData);
        System.out.println(currData.GetData());
        currData = new SiblingData(currData);
        System.out.println(currData.GetData());
    }
}
