package Decorater;

/**
 * Created by Deniz on 12.04.2016.
 */
public class SiblingData extends SampleDataDecorater {
    public SiblingData(SampleData sampleData) {
        super(sampleData);
    }

    @Override
    String GetData() {
        return sampleData.GetData() + " " + "Kardeşi var";
    }
}
