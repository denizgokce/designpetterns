package Decorater;

/**
 * Created by Deniz on 11.04.2016.
 */
public abstract class SampleDataDecorater extends SampleData {

    protected SampleData sampleData;

    public SampleDataDecorater(SampleData sampleData) {
        this.sampleData = sampleData;
    }
}
