package Prototype;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Client {
    public static void main(String[] args) {
        ShapeCache shapeMenu = new  ShapeCache();
        shapeMenu.loadCache();

        Shape clonedShape = (Shape) shapeMenu.getShape("1");
        System.out.println("Shape : " + clonedShape.getType());
        clonedShape.draw();

        Shape clonedShape2 = (Shape) shapeMenu.getShape("2");
        System.out.println("Shape : " + clonedShape2.getType());
        clonedShape2.draw();

        Shape clonedShape3 = (Shape) shapeMenu.getShape("3");
        System.out.println("Shape : " + clonedShape3.getType());
        clonedShape3.draw();
    }
}
