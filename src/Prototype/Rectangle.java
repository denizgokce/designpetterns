package Prototype;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Rectangle extends Shape {

    public Rectangle(){
        type = "Rectangle";
    }
    @Override
    public void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}
