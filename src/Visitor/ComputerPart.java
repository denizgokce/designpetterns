package Visitor;

/**
 * Created by Deniz on 6.06.2016.
 */
public interface ComputerPart {
    public void accept(ComputerPartVisitor computerPartVisitor);
}
