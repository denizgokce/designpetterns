package Visitor;

/**
 * Created by Deniz on 6.06.2016.
 */
public class Client {
    public static void main(String[] args) {
        ComputerPart computer = new Computer();
        computer.accept(new ComputerPartDisplayVisitor());
    }
}
