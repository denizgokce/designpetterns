package Template;

/**
 * Created by Deniz on 13.04.2016.
 */
public abstract class AbstractObject {
    public abstract void PrimitiveOperation1();

    public abstract void PrimitiveOperation2();

    public void TemplateMethod() {
        PrimitiveOperation1();
        PrimitiveOperation2();
    }
}
