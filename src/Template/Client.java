package Template;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        AbstractObject object1 = new Object1();
        object1.TemplateMethod();
        AbstractObject object2 = new Object2();
        object2.TemplateMethod();
    }
}
