package Template.LabWork;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.util.Random;

/**
 * Created by Deniz on 18.04.2016.
 */
public abstract class myRobot {
    protected abstract String setProcessName();

    protected abstract int[] defineInput();

    public final void start() {
        try {
            int[] inputs = convertToKeyPress(defineInput());
            String procName = setProcessName();
            Process p = Runtime.getRuntime().exec(procName);
            Thread.sleep(3000);
            Robot robot = new Robot();
            for (int i = 0; i < inputs.length; i++) {
                robot.delay(100);
                robot.keyPress(inputs[i]);
            }
            p.destroy();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    private int[] convertToKeyPress(int[] numbers) throws Exception {
        String keyCodes = "";
        for (int i = 0; i < numbers.length; i++) {
            String[] res = String.valueOf(numbers[i]).split("");
            for (int j = 0; j < res.length; j++) {
                keyCodes += "VK_" + res[j] + " ";
            }
            keyCodes += "VK_SPACE" + " ";
        }
        String[] keyPressString = keyCodes.split(" ");
        int[] keys = new int[keyPressString.length];
        for (int i = 0; i < keyPressString.length; i++) {
            Field f = KeyEvent.class.getField(keyPressString[i]);
            keys[i] = f.getInt(null);
        }
        return keys;
    }

    public static int randInt(int min, int max) {

        Random rand = new Random();


        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
