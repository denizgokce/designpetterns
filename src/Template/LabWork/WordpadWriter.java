package Template.LabWork;

/**
 * Created by Deniz on 18.04.2016.
 */
public class WordpadWriter extends myRobot {
    @Override
    protected String setProcessName() {
        return "cmd /c start wordpad";
    }

    @Override
    protected int[] defineInput() {
        int[] IntArray = new int[74];
        for (int i = 0; i < 74; i++) {
            IntArray[i] = this.randInt(100, 350);
        }
        return IntArray;
    }
}
