package Template.LabWork;

/**
 * Created by Deniz on 18.04.2016.
 */
public class NotepadWriter extends myRobot {
    @Override
    protected String setProcessName() {
        return "C:\\Windows\\notepad.exe C:\\Users\\Deniz\\IdeaProjects\\designPatterns\\src\\Template\\LabWork\\file.txt";
    }

    @Override
    protected int[] defineInput() {
        int[] IntArray = new int[24];
        for (int i = 0; i < 24; i++) {
            IntArray[i] = this.randInt(1, 99);
        }
        return IntArray;
    }
}
