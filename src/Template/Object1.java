package Template;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Object1 extends AbstractObject {
    @Override
    public void PrimitiveOperation1() {
        System.out.println(this.getClass().getSimpleName() + " this class's PrimitiveOperation1 has done");
    }

    @Override
    public void PrimitiveOperation2() {
        System.out.println(this.getClass().getSimpleName() + " this class's PrimitiveOperation2 has done");
    }
}
