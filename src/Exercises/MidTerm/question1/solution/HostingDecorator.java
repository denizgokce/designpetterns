package Exercises.MidTerm.question1.solution;

/**
 * Created by dindar.oz on 31.03.2016.
 */
public abstract class HostingDecorator implements IHostingServices {

    IHostingServices hostingServices;

    public HostingDecorator(IHostingServices hostingServices) {
        this.hostingServices = hostingServices;
    }

    @Override
    public double cost() {
        return hostingServices.cost();
    }


}
