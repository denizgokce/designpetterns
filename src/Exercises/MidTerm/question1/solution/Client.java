package Exercises.MidTerm.question1.solution;

/**
 * Created by dindar.oz on 31.03.2016.
 */
public class Client {
    public static void main(String[] args) {
        IHostingServices hostingService = new BaseService();
        System.out.println(String.valueOf(hostingService.cost()));
        hostingService = new FlashMedia(hostingService);
        hostingService = new MySql(hostingService);
        System.out.println(String.valueOf(hostingService.cost()));
    }
}
