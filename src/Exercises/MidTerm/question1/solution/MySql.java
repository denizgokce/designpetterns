package Exercises.MidTerm.question1.solution;

/**
 * Created by dindar.oz on 31.03.2016.
 */
public class MySql extends HostingDecorator {
    public MySql(IHostingServices hostingServices) {
        super(hostingServices);
    }

    @Override
    public double cost() {
        return super.cost()+30;
    }
}
