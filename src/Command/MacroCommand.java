package Command;

/**
 * Created by Deniz on 12.04.2016.
 */
public class MacroCommand implements ICommand {
    private Device device = null;

    public MacroCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.doActionA();
        device.doActionB();
        device.doActionC();
        System.out.println(this.getClass().getSimpleName() + " This command ends in here..");
    }

    @Override
    public void undo() {

    }
}
