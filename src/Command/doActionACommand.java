package Command;

/**
 * Created by Deniz on 12.04.2016.
 */
public class doActionACommand implements ICommand {
    private Device device = null;

    public doActionACommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.doActionA();
        System.out.println(this.getClass().getSimpleName() + " This command ends in here..");
    }

    @Override
    public void undo() {

    }
}
