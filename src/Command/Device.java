package Command;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Device {
    public void doActionA() {
        System.out.println("Device done the action A");
    }

    public void doActionB() {
        System.out.println("Device done the action B");
    }

    public void doActionC() {
        System.out.println("Device done the action C");
    }
}
