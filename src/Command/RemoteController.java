package Command;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deniz on 12.04.2016.
 */
public class RemoteController {
    List<ICommand> commandList = null;

    public RemoteController() {
        this.commandList = new ArrayList<>();
    }

    public void addCommand(ICommand command) {
        commandList.add(command);
    }

    public void runCommand(int slot) {
        commandList.get(slot).execute();
    }
}
