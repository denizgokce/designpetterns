package Command;

/**
 * Created by Deniz on 12.04.2016.
 */
public interface ICommand {
    void execute();

    void undo();
}
