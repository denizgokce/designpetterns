package Command;

/**
 * Created by Deniz on 12.04.2016.
 */
public class doActionBCommand implements ICommand {
    private Device device = null;

    public doActionBCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.doActionB();
        System.out.println(this.getClass().getSimpleName() + " This command ends in here..");
    }

    @Override
    public void undo() {

    }
}
