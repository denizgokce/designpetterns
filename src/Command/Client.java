package Command;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        Device device = new Device();
        RemoteController remoteController = new RemoteController();

        remoteController.addCommand(new doActionACommand(device));
        remoteController.addCommand(new MacroCommand(device));

        remoteController.runCommand(0);
        remoteController.runCommand(1);
    }
}
