package Singleton;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        Singleton.GetInstance().setData("Göktuğ naber?");
        /**
         * A lot of actions happens in here..
         */
        System.out.println(Singleton.GetInstance().getData());
    }
}
