package Observer;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Observer implements IObserver {
    @Override
    public void update(String notification) {
        System.out.println("Notification Recieved From ISubject : \"" + notification + "\"");
    }
}
