package Observer;

/**
 * Created by Deniz on 11.04.2016.
 */
public interface ISubject {
    void notifyObservers();

    void registerObserver(IObserver observer);

    void unregisterObserver(IObserver observer);

    void setImpartantData(String importantData);

    String getImpartantData();
}
