package Observer;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Client {
    public static void main(String[] args) {

        ISubject subject = new Subject();
        subject.registerObserver(new Observer());
        subject.registerObserver(new Observer());

        subject.setImpartantData("Yeni Bildirim");
    }
}
