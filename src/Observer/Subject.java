package Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Subject implements ISubject {

    List<IObserver> observerList = null;

    public String getImpartantData() {
        return impartantData;
    }

    public void setImpartantData(String impartantData) {
        this.impartantData = impartantData;
        notifyObservers();
    }

    public String impartantData = "";

    public Subject() {
        this.observerList = new ArrayList<IObserver>();
    }

    @Override
    public void notifyObservers() {
        for (IObserver o : observerList) {
            o.update(impartantData);
        }
    }

    @Override
    public void registerObserver(IObserver observer) {
        this.observerList.add(observer);
    }

    @Override
    public void unregisterObserver(IObserver observer) {
        this.observerList.remove(observer);
    }

}
