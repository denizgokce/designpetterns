package Observer;

/**
 * Created by Deniz on 11.04.2016.
 */
public interface IObserver {
    void update(String notification);
}
