package Proxy;

/**
 * Created by Deniz on 5.06.2016.
 */
public interface ISubject {
    void doAction();
}
