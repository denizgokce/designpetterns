package Proxy;

/**
 * Created by Deniz on 5.06.2016.
 */
public class ProxySubject implements ISubject {

    ISubject realSubject;

    public ProxySubject(ISubject realSubject) {
        this.realSubject = realSubject;
    }

    @Override
    public void doAction() {
        System.out.println("Pre defined job before real subject action.");
        this.realSubject.doAction();
        System.out.println("Post defined jobs after real subject action.");
    }
}
