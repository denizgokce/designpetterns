package Proxy;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Client {
    public static void main(String[] args) {
        ISubject realSubject = new RealSubject();
        //realSubject.doAction();
        ISubject proxySubject = new ProxySubject(realSubject);
        proxySubject.doAction();
    }
}
