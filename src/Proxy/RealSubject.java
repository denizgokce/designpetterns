package Proxy;

/**
 * Created by Deniz on 5.06.2016.
 */
public class RealSubject implements ISubject{
    @Override
    public void doAction() {
        System.out.println("This method inserted a new user.");
    }
}
