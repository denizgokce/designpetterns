package Mediator;

/**
 * Created by Deniz on 6.06.2016.
 */
public class ConcreteColleague2 extends Colleague {
    public ConcreteColleague2(Mediator mediator) {
        super(mediator);
    }
    @Override
    public void Send(String message) {
        mediator.Send(message, this);
    }

    @Override
    public void Notify(String message) {
        System.out.println("Colleague2 gets message: " + message);
    }
}
