package Mediator;

/**
 * Created by Deniz on 6.06.2016.
 */
public class ConcreteColleague1 extends Colleague {
    public ConcreteColleague1(Mediator mediator) {
        super(mediator);
    }
    @Override
    public void Send(String message) {
        mediator.Send(message, this);
    }

    @Override
    public void Notify(String message) {
        System.out.println("Colleague1 gets message: " + message);
    }
}
