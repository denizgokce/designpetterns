package Mediator;

/**
 * Created by Deniz on 6.06.2016.
 */
public abstract class Mediator {
    public abstract void Send(String message, Colleague colleague);
}
