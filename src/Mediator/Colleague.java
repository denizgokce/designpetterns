package Mediator;

/**
 * Created by Deniz on 6.06.2016.
 */
public abstract class Colleague {
    protected Mediator mediator;

    public Colleague(Mediator mediator) {
        this.mediator = mediator;
    }

    public abstract void Notify(String message);
    public abstract void Send(String message);
}
