package Mediator;

/**
 * Created by Deniz on 6.06.2016.
 */
public class ConreteMediator extends Mediator {

    public Colleague concreteColleague1;
    public Colleague concreteColleague2;

    public ConreteMediator() {

    }


    @Override
    public void Send(String message, Colleague colleague) {
        if (colleague.equals(this.concreteColleague1)) {
            this.concreteColleague2.Notify(message);
        } else {
            this.concreteColleague1.Notify(message);
        }
    }
}
