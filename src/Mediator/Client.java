package Mediator;

/**
 * Created by Deniz on 6.06.2016.
 */
public class Client {
    public static void main(String[] args) {
        ConreteMediator mediator = new ConreteMediator();

        Colleague user1 = new ConcreteColleague1(mediator);
        Colleague user2 = new ConcreteColleague2(mediator);

        mediator.concreteColleague1 = user1;
        mediator.concreteColleague2 = user2;

        user1.Send("Hi how are you doing?");
        user2.Send("Good! You?");


    }
}
