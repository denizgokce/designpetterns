package ChainOfResponsibility;

/**
 * Created by Deniz on 6.06.2016.
 */
public abstract class Handler {
    protected Handler successor;

    public void SetSuccessor(Handler successor) {
        this.successor = successor;
    }

    public abstract void HandleRequest(int request);
}
