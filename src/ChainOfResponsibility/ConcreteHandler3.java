package ChainOfResponsibility;

/**
 * Created by Deniz on 6.06.2016.
 */
public class ConcreteHandler3 extends Handler {
    @Override
    public void HandleRequest(int request) {
        if (request >= 20 && request < 30) {
            System.out.println(this.getClass().getSimpleName() + " handled request " + request);
        } else if (successor != null) {
            successor.HandleRequest(request);
        }
    }
}
