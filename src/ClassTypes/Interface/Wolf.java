package ClassTypes.Interface;

import ClassTypes.Mammet;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Wolf extends Mammet implements Carnivor {
    @Override
    public void breath() {
        System.out.println("this animal breathes");
    }

    @Override
    public void eat() {
        System.out.println("This wolf eats what it hunts");
    }
}
