package ClassTypes.Interface;

import ClassTypes.Mammet;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Dog extends Mammet implements Carnivor {
    @Override
    public void breath() {
        System.out.println("this animal breathes");
    }

    @Override
    public void eat() {
        System.out.println("This Dog eats what given to it");
    }
}
