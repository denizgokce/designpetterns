package ClassTypes.Abstract;

import ClassTypes.Mammet;

/**
 * Created by Deniz on 11.04.2016.
 */
public abstract class Carnivor extends Mammet {
    public boolean doesHaveTooth = true;
    public void breath(){
        System.out.println("this animal breathes");
    }
    public abstract void eat();
}
