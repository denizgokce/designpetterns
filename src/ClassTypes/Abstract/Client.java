package ClassTypes.Abstract;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        Carnivor carnivorDog = new Dog();
        Carnivor carnivorWolf = new Wolf();

        carnivorDog.breath();
        carnivorDog.eat();

        carnivorWolf.breath();
        carnivorWolf.eat();


    }
}
