package ClassTypes.Abstract;

/**
 * Created by Deniz on 11.04.2016.
 */
public class Wolf extends Carnivor {
    @Override
    public void eat() {
        System.out.println("This wolf eats what it hunts");
    }
}
