package Builder;

/**
 * Created by Deniz on 5.06.2016.
 */
public class CarBuilder implements IBuilder {

    Product product;

    public CarBuilder() {
        product = new Product();
    }

    @Override
    public void BuildPart() {
        product.addPart("Wheels");
        product.addPart("Seats");
        product.addPart("Windows");
        product.addPart("Shift");
        product.addPart("Engine");
    }

    @Override
    public Product GetResult() {
        return this.product;
    }
}
