package Builder;

/**
 * Created by Deniz on 5.06.2016.
 */
public class TruckBuilder implements IBuilder {

    Product product;

    public TruckBuilder() {
        product = new Product();
    }

    @Override
    public void BuildPart() {
        product.addPart("Wheels");
        product.addPart("Seats");
        product.addPart("Windows");
        product.addPart("Shift");
        product.addPart("Engine");
        product.addPart("Case");
        product.addPart("MoreWheels");
    }

    @Override
    public Product GetResult() {
        return this.product;
    }
}
