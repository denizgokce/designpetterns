package Builder;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Client {
    public static void main(String[] args) {
        Director director = new Director(new CarBuilder());
        director.Construct();
        System.out.println(director.getBuilder().GetResult().toString());
        director.setBuilder(new TruckBuilder());
        director.Construct();
        System.out.println(director.getBuilder().GetResult().toString());
    }
}
