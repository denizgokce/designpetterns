package Builder;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Director {

    private IBuilder builder;

    public Director(IBuilder builder) {
        this.builder = builder;
    }

    public IBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(IBuilder builder) {
        this.builder = builder;
    }

    public void Construct(){
        this.builder.BuildPart();
    }

}
