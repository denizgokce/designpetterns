package Builder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Product {

    List<String> partList;

    public Product() {
        this.partList = new ArrayList<String>();
    }

    public void addPart(String part) {
        this.partList.add(part);
    }

    public String toString() {
        String result = "This product is made of {\n";
        for (String part : partList) {
            result += part + " ,\n";
        }
        result += "}";
        return result;
    }
}
