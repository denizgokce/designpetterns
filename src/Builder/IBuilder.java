package Builder;

/**
 * Created by Deniz on 5.06.2016.
 */
public interface IBuilder {
    void BuildPart();
    Product GetResult();
}
