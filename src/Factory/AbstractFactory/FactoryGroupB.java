package Factory.AbstractFactory;

/**
 * Created by Deniz on 12.04.2016.
 */
public class FactoryGroupB implements IFactory {
    @Override
    public IObject create(String object) {
        switch (object) {
            case "Object3":
                return new Object3();
            case "Object4":
                return new Object4();
            default:
                return null;
        }
    }
}
