package Factory.AbstractFactory;

/**
 * Created by Deniz on 12.04.2016.
 */
public class FactoryGroupA implements IFactory{
    @Override
    public IObject create(String object) {
        switch (object) {
            case "Object":
                return new Object1();
            case "Object2":
                return new Object2();
            default:
                return null;
        }
    }
}
