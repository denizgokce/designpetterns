package Factory.AbstractFactory;

/**
 * Created by Deniz on 12.04.2016.
 */
public interface IObject {
    void printState();
}
