package Factory.AbstractFactory;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Object2 implements IObject {
    @Override
    public void printState() {
        System.out.println("This object is part of GroupA and name is " + this.getClass().getSimpleName());
    }
}
