package Factory.AbstractFactory;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        IFactory factoryA = new FactoryGroupA();
        IFactory factoryB = new FactoryGroupB();

        IObject sampleObject = factoryA.create("Object2");
        IObject sampleObject2 = factoryB.create("Object4");

        sampleObject.printState();
        sampleObject2.printState();
    }
}
