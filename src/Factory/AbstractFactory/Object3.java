package Factory.AbstractFactory;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Object3 implements IObject {
    @Override
    public void printState() {
        System.out.println("This object is part of GroupB and name is " + this.getClass().getSimpleName());
    }
}
