package Factory.FactoryMethod;

/**
 * Created by Deniz on 12.04.2016.
 */
public class ObjectFactory {
    public IObject Create(String object) {
        switch (object) {
            case "Object":
                return new Object1();
            case "Object2":
                return new Object2();
            case "Object3":
                return new Object3();
            default:
                return null;
        }
    }
}
