package Factory.FactoryMethod;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Object3 implements IObject {
    @Override
    public void printState() {
        System.out.println("This Object is " + this.getClass().toString());
    }
}
