package Factory.FactoryMethod;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        ObjectFactory objectFactory = new ObjectFactory();
        IObject object = objectFactory.Create("Object2");
        object.printState();
    }
}
