package Iterator;

/**
 * Created by Deniz on 13.04.2016.
 */
public class DataIterator implements _Iterator {
    String[] data = null;

    int index;

    public DataIterator(String[] data) {
        this.data = data;
    }

    @Override
    public boolean hasNext() {
        if (index < data.length) {
            return true;
        }
        return false;
    }

    @Override
    public Object next() {
        if (this.hasNext()) {
            return data[index++];
        }
        return null;
    }
}
