package Iterator;

/**
 * Created by Deniz on 13.04.2016.
 */
public interface _Iterator {
    boolean hasNext();

    Object next();
}
