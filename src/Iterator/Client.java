package Iterator;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        DataRepository dataRepository = new DataRepository();
        int index = 1;
        for (_Iterator iter = dataRepository.getIterator(); iter.hasNext(); ) {
            String data = (String) iter.next();
            System.out.println(String.valueOf(index) + ". Data : " + data);
            index++;
        }
    }
}
