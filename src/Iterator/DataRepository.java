package Iterator;

/**
 * Created by Deniz on 13.04.2016.
 */
public class DataRepository implements Container {
    String[] data = {"Data1", "Data2", "Data3"};

    @Override
    public _Iterator getIterator() {
        return new DataIterator(data);
    }

}
