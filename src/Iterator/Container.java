package Iterator;

/**
 * Created by Deniz on 13.04.2016.
 */
public interface Container {
    _Iterator getIterator();
}
