package ObjectPool;

/**
 * Created by Deniz on 6.06.2016.
 */
public class Object {
    private String name;

    public Object(String name) {
        this.name = name;
    }

    public void display() {
        System.out.println(this.name + " is Used!!");
    }
}
