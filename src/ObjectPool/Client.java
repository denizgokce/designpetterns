package ObjectPool;

/**
 * Created by Deniz on 6.06.2016.
 */
public class Client {
    public static void main(String[] args) {

        Object object1 = ObjectPool.GetInstace().acquireObject();
        object1.display();

        Object object2 = ObjectPool.GetInstace().acquireObject();
        object2.display();

        ObjectPool.GetInstace().releaseObject(object1);
        ObjectPool.GetInstace().releaseObject(object2);

        Object object3 = ObjectPool.GetInstace().acquireObject();
        object3.display();
        Object object4 = ObjectPool.GetInstace().acquireObject();
        object4.display();

    }
}
