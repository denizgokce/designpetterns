package ObjectPool;


import java.util.Stack;
import java.util.StringJoiner;

/**
 * Created by Deniz on 6.06.2016.
 */
public class ObjectPool {

    Stack<Object> objectList;

    private int objectCounter = 0;

    private static ObjectPool instance;

    private ObjectPool() {
        this.objectList = new Stack<Object>();
    }

    public static ObjectPool GetInstace() {
        if (instance == null) {
            instance = new ObjectPool();
        }
        return instance;
    }

    public Object acquireObject() {
        if (this.objectList.size() == 0) {
            objectCounter++;
            return new Object("Object" + String.valueOf(objectCounter));
        } else {
            return this.objectList.pop();
        }
    }

    public void releaseObject(Object object) {
        this.objectList.push(object);
    }
}
