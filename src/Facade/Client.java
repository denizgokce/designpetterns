package Facade;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Client {
    public static void main(String[] args) {

        FacadeObject facadeObject = new FacadeObject();

        facadeObject.runObject1Action();
        facadeObject.runObject2Action();
        facadeObject.runObject3Action();
    }
}
