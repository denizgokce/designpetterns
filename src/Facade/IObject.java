package Facade;

/**
 * Created by Deniz on 13.04.2016.
 */
public interface IObject {
    void doAction();
}
