package Facade;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Object3 implements IObject {
    @Override
    public void doAction() {
        System.out.println(this.getClass().getSimpleName() + " class does its action");
    }
}
