package Facade;

/**
 * Created by Deniz on 13.04.2016.
 */
public class FacadeObject {
    private IObject object1 = null;
    private IObject object2 = null;
    private IObject object3 = null;

    public FacadeObject() {
        this.object1 = new Object1();
        this.object2 = new Object2();
        this.object3 = new Object3();
    }

    public void runObject1Action() {
        object1.doAction();
        object2.doAction();
    }

    public void runObject2Action() {
        object2.doAction();
    }

    public void runObject3Action() {
        object3.doAction();
    }
}
