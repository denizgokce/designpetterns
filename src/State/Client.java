package State;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Client {
    public static void main(String[] args) {
        Context object = new Context(new StartState());
        object.doStateAction();
        object.setState(new StopState());
        object.doStateAction();
    }
}
