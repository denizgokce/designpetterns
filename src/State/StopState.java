package State;

/**
 * Created by Deniz on 5.06.2016.
 */
public class StopState implements IState {
    @Override
    public void doAction() {
        System.out.println("Context is in " + this.getClass().getSimpleName());
    }
}