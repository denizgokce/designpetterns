package State;

/**
 * Created by Deniz on 5.06.2016.
 */
public interface IState {
    void doAction();
}
