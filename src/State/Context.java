package State;

/**
 * Created by Deniz on 5.06.2016.
 */
public class Context {

    IState state;

    public Context(IState state) {
        this.state = state;
    }

    public IState getState() {
        return state;
    }

    public void setState(IState state) {
        this.state = state;
    }
    public void doStateAction(){
        this.state.doAction();
    }
}
