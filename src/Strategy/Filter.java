package Strategy;


public class Filter {

    private CheckStrategy myStrategy;

    public void setFilter(CheckStrategy myStrategy) {
        this.myStrategy = myStrategy;
    }

    public void ApplyFilter(String data) {
        myStrategy.check(data);
    }

}
