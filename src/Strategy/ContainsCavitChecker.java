package Strategy;

/**
 * Created by Deniz on 12.04.2016.
 */
public class ContainsCavitChecker implements CheckStrategy {
    @Override
    public boolean check(String s) {
        if (s.contains("Cavit")) {
            System.out.println("This text has cavit..");
            return true;
        } else {
            System.out.println("This text does not have caivt..");
            return false;
        }

    }
}
