package Strategy;


public interface CheckStrategy {
    boolean check(String s);
}
