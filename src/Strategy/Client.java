package Strategy;

/**
 * Created by Deniz on 12.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        Filter myContext = new Filter();

        myContext.setFilter(new LengthChecker());
        myContext.ApplyFilter("Deniz bu yazıyı 20 karakterden büyük mü diye test ediyor..");

        myContext.setFilter(new ContainsCavitChecker());
        myContext.ApplyFilter("Cavit burdaydı..");
    }
}
