package Strategy;

/**
 * Created by Deniz on 12.04.2016.
 */
public class LengthChecker implements CheckStrategy {
    @Override
    public boolean check(String s) {
        if (s.length() < 20) {
            System.out.println("Text Length is smaller than 20 character.");
            return true;
        } else {
            System.out.println("Text Length is bigger than 20 character.");
            return false;
        }
    }
}
