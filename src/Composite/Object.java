package Composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Object {
    /**
     * Some sample information here...
     */
    public String data1 = null;
    public String data2 = null;
    public List<Object> subObjects = null;

    public Object(String data1, String data2) {
        this.data1 = data1;
        this.data2 = data2;
        this.subObjects = new ArrayList<>();
    }

    public void addSubObjects(Object object) {
        subObjects.add(object);
    }

    public List<Object> getSubObjects() {
        return subObjects;
    }

    public void print() {
        System.out.println(data1 + " " + data2);
    }
}
