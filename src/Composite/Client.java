package Composite;

/**
 * Created by Deniz on 13.04.2016.
 */
public class Client {
    public static void main(String[] args) {
        Object object = new Object("object1", "root");
        Object object1 = new Object("object2", "root");

        Object object2 = new Object("Object.1", "subMember");
        Object object3 = new Object("Object.2", "subMember");

        Object object4 = new Object("Object2.1", "subMember");
        Object object5 = new Object("Object2.1", "subMember");

        object.addSubObjects(object2);
        object.addSubObjects(object3);

        object1.addSubObjects(object4);
        object1.addSubObjects(object5);

        object.print();
        for (Object o : object.getSubObjects()) {
            o.print();
        }
        object1.print();
        for (Object o : object1.getSubObjects()) {
            o.print();
        }

    }
}
